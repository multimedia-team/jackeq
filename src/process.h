#ifndef PROCESS_H
#define PROCESS_H

#include <jack/jack.h>

#include "djeq.h"

/* number of input and output channels */
#define NCHANNELS 8
/* number of DJ EQ plugins. Must be 1/2 of NCHANNELS */
#define N_PLUGINS 4

#define CHANNEL_L 0
#define CHANNEL_R 1

extern float mast_peak[];
extern float mon_peak[];
extern float out_peak[];
extern float aux_peak[];
extern float djeq_peak[];
extern float aux_gain[];
extern float aux_gain_target[];
extern float chan_gain[];
extern float chan_gain_target[];
extern float fader_gain[];
extern float fader_gain_target[];
extern float mast_gain;
extern float mast_gain_target;
extern float mon_gain;
extern float mon_gain_old;
extern float mon_gain_target;


void process_init();

extern djeq_settings djeq[N_PLUGINS];
extern int global_bypass[];
extern int global_mon[];
extern int global_aux1[];
extern int global_aux2[];
extern int global_group[][6];

extern jack_port_t *input_ports[],
                   *output_ports[],
		   *aux_return_input_ports[],
		   *aux_return_output_ports[],
                   *aux_send_output_ports[],
                   *mast_output_ports[],
                   *mon_output_ports[];

extern jack_client_t *client;

#endif
