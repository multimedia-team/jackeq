#include <stdio.h>
#include <gtk/gtk.h>
#include <math.h>

#include "process.h"
#include "support.h"
#include "main.h"
#include "gtkmeter.h"
#include "db.h"

static GtkMeter *out_meter[2];
static GtkAdjustment  *out_meter_adj[2];


void bind_mon()
{
    GtkWidget *slider;
    
    slider = lookup_widget(main_window, "monitor"); 
    gtk_range_set_value(GTK_RANGE(slider), -10.0);

    out_meter[0] = GTK_METER(lookup_widget(main_window, "monmeter_l"));
    out_meter[1] = GTK_METER(lookup_widget(main_window, "monmeter_r"));
    out_meter_adj[0] = gtk_meter_get_adjustment(out_meter[0]);
    out_meter_adj[1] = gtk_meter_get_adjustment(out_meter[1]);
    gtk_adjustment_set_value(out_meter_adj[0], -70.0);
    gtk_adjustment_set_value(out_meter_adj[1], -70.0);

}

void mon_out_meter_value(float amp[])
{
    gtk_adjustment_set_value(out_meter_adj[0], lin2db(amp[0]));
    gtk_adjustment_set_value(out_meter_adj[1], lin2db(amp[1]));
    amp[0] = 0.0f;
    amp[1] = 0.0f;
}

void mon_gain_changed(GtkAdjustment *adjustment, gpointer user_data)
{
	mon_gain_target = adjustment->value;
}



/* vi:set ts=8 sts=4 sw=4: */
