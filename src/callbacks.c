#ifdef HAVE_CONFIG_H
#  include "config.h"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <sys/times.h>
#include <errno.h>
#include <math.h>
#include <string.h>
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>


#include "main.h"
#include "callbacks.h"
#include "callbacks_help.h"
#include "interface.h"
#include "support.h"
#include "process.h"
#include "gtkmeter.h"
#include "gtkmeterscale.h"
#include "aux-ui.h"
#include "djeq-ui.h"
#include "master-ui.h"
#include "monitor-ui.h"
#include "db.h"
#include "help.h"


#define MAIN_BUTTONS             0
#define INPUT                    1
#define EQ_OPTIONS               3
#define LO                       4
#define MID                      5
#define HIGH                     6
#define OUTPUT                   8

/* Convert from dB's to a coefficient */
#define DB_CO(g) powf(10.0f, (g) * 0.05f)

static char *help_ptr = general_help;
static GtkWidget *about_window;
static GtkWidget *fileChooser;
static GtkWidget *messageBox;

gboolean
on_window1_delete_event                (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data)
{
    commitSettings();
    gtk_main_quit();

    return FALSE;
}

void
on_quit1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{


    commitSettings();
    gtk_main_quit();

}


void
on_about1_activate                     (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
    GtkLabel *version_label;
    about_window = create_window2();
    
    version_label = GTK_LABEL(lookup_widget(about_window, "lblVersion"));

    gtk_label_set_label(version_label, PACKAGE " " VERSION "\nhttp://djcj.org/jackeq\n\n"
    "(c) 2003 - 2009 P. Shirkey\n\n"
    "Contributions from:\n S. Harris, J. O'Quin\n\n"
    "This is free software, and you are welcome \nto redistribute it under certain conditions;\n"
    "see the file COPYING for details.\n");
	
    gtk_widget_show(about_window);

}


void
on_open2_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

    gchar *tempfilepath;
    fileChooser = create_filechooserdialog1();
    gtk_widget_show(fileChooser);
    
    switch (gtk_dialog_run (GTK_DIALOG (fileChooser)))
    {
    case GTK_RESPONSE_OK:
      /* get the path */
      tempfilepath = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (fileChooser));
      g_print(_("loading filepath = %s\n"), tempfilepath); 
      g_snprintf(filepath, 255, "%s",tempfilepath);
      getSettings();
      gtk_widget_destroy (fileChooser);
      break;
    default:
      gtk_widget_destroy (fileChooser);
      return;
    }


}


void
on_save2_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
    gchar *tempfilepath;
    
    fileChooser = create_filechooserdialog2();
//    gtk_file_chooser_set_action (GTK_FILE_CHOOSER (fileChooser), GTK_FILE_CHOOSER_ACTION_SAVE);
    gtk_widget_show(fileChooser);
    
    switch (gtk_dialog_run (GTK_DIALOG (fileChooser)))
    {
    case GTK_RESPONSE_OK:
      /* get the path */
      tempfilepath = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (fileChooser));
      g_print(_(" saving filepath = %s\n"), tempfilepath);
      g_snprintf(filepath, 255, "%s",tempfilepath);
      commitSettings();
      gtk_widget_destroy (fileChooser);
      break;
    default:
      gtk_widget_destroy (fileChooser);
      return;
    }
    
}


/*
void 
knob_slider_handler  (GtkWidget *adj, Control *c) {
  control_emit(c, GTK_ADJUSTMENT(adj)->value);
}
*/

GtkWidget*
make_meter (gchar *widget_name, gchar *string1, gchar *string2,
                gint int1, gint int2)
{
    GtkWidget *ret;
    gint dir = GTK_METER_UP;
    int sides = 0;
    GtkAdjustment *adjustment = (GtkAdjustment*) gtk_adjustment_new (0.0,
		    (float)int1, (float)int2, 0.0, 0.0, 0.0);

 /*   if (!string1 || !strcmp(string1, "up")) {
        dir = GTK_METER_UP;
    } else if (!strcmp(string1, "down")) {
	dir = GTK_METER_DOWN;
    } else if (!strcmp(string1, "left")) {
	dir = GTK_METER_LEFT;
    } else if (!strcmp(string1, "right")) {
	dir = GTK_METER_RIGHT;
    }
  */  
    
    if (string1 && strstr(string1, "left")) {
	sides |= GTK_METERSCALE_LEFT;
    }
    if (string1 && strstr(string1, "right")) {
	sides |= GTK_METERSCALE_RIGHT;
    }
    if (string1 && strstr(string1, "top")) {
	sides |= GTK_METERSCALE_TOP;
    }
    if (string1 && strstr(string1, "bottom")) {
	sides |= GTK_METERSCALE_BOTTOM;
    }

    ret = gtk_meter_new(adjustment, dir, sides, int1, int2);

    return ret;
}


GtkWidget*
make_mscale (gchar *widget_name, gchar *string1, gchar *string2,
                gint int1, gint int2)
{
    int sides = 0;
    GtkWidget *ret;

    if (string1 && strstr(string1, "left")) {
	sides |= GTK_METERSCALE_LEFT;
    }
    if (string1 && strstr(string1, "right")) {
	sides |= GTK_METERSCALE_RIGHT;
    }
    if (string1 && strstr(string1, "top")) {
	sides |= GTK_METERSCALE_TOP;
    }
    if (string1 && strstr(string1, "bottom")) {
	sides |= GTK_METERSCALE_BOTTOM;
    }

    ret = gtk_meterscale_new(sides, int1, int2);

    return ret;
}

void
on_Hi_scale_value_changed              (GtkRange        *range,
                                        gpointer         user_data)
{
    int i;
    GtkRange *slider;
    char name[256];
    GtkAdjustment *a[N_PLUGINS]; 

    for (i=0; i<N_PLUGINS; i++) {
	snprintf(name, 255, "djeq%i_1", i+1);
	slider = lookup_widget(main_window, name); 
        a[i] = gtk_range_get_adjustment(slider);
	djeq[i].hi = a[i]->value;
    }
}


void
on_Mid_scale_value_changed             (GtkRange        *range,
                                        gpointer         user_data)
{
    int i;
    GtkRange *slider;
    char name[256];
    GtkAdjustment *a[N_PLUGINS]; 

    for (i=0; i<N_PLUGINS; i++) {
	snprintf(name, 255, "djeq%i_2", i+1);
	slider = lookup_widget(main_window, name); 
        a[i] = gtk_range_get_adjustment(slider);
	djeq[i].mid = a[i]->value;
    }
}


void
on_Lo_scale_value_changed              (GtkRange        *range,
                                        gpointer         user_data)
{
    int i;
    GtkRange *slider;
    char name[256];
    GtkAdjustment *a[N_PLUGINS]; 
    
    for (i=0; i<N_PLUGINS; i++) {
	snprintf(name, 255, "djeq%i_3", i+1);
	slider = lookup_widget(main_window, name); 
        a[i] = gtk_range_get_adjustment(slider);
	djeq[i].lo = a[i]->value;
    }
}

void
on_channel_gain_scale_value_changed    (GtkRange        *range,
                                        gpointer         user_data)
{
    int i;
    GtkRange *slider;
    char name[256];
    GtkAdjustment *a[N_PLUGINS]; 
    
    for (i=0; i<N_PLUGINS; i++) {
	snprintf(name, 255, "channel_gain_%i", i+1);
	slider = lookup_widget(main_window, name); 
        a[i] = gtk_range_get_adjustment(slider);
        chan_gain_target[i] = DB_CO(a[i]->value);
//	printf ("adj chan%i\n", i);
    }
}


void
on_fader_scale_value_changed           (GtkRange        *range,
                                        gpointer         user_data)
{
    int i;
    GtkRange *slider;
    char name[256];
    GtkAdjustment *a[N_PLUGINS]; 
    
    for (i=0; i<2; i++) {
	snprintf(name, 255, "fader_%i", i+1);
	slider = lookup_widget(main_window, name); 
        a[i] = gtk_range_get_adjustment(slider);
        fader_gain_target[i] = a[i]->value;
//	printf ("adj fader%i\n", i);
    }

}


void
on_aux_gain_scale_value_changed        (GtkRange        *range,
                                        gpointer         user_data)
{
    int i;
    GtkRange *slider;
    char name[256];
    GtkAdjustment *a[2]; 
    
    for (i=0; i<2; i++) {
	snprintf(name, 255, "aux_gain_%i", i+1);
	slider = lookup_widget(main_window, name); 
        a[i] = gtk_range_get_adjustment(slider);
        aux_gain_target[i] = DB_CO(a[i]->value);
//	printf ("adj aux_%i\n", i);
    }

}
 
void
on_master_scale_value_changed          (GtkRange        *range,
                                        gpointer         user_data)
{
    GtkAdjustment *a = gtk_range_get_adjustment(range);
    mast_gain_target = DB_CO(a->value);
}

void
on_mon_scale_value_changed             (GtkRange        *range,
                                        gpointer         user_data)
{

    GtkAdjustment *a = gtk_range_get_adjustment(range);
    mon_gain_target = DB_CO(a->value);

}


void
on_mon_button_toggled                  (GtkToggleButton *togglebutton,
                                        gpointer         user_data)
{
    int i;
    GtkToggleButton *button;
    char name[256];
    
    for (i=0; i < N_PLUGINS +2; i++) {
	snprintf(name, 255, "mon_button_%i", i+1);
	button = lookup_widget(main_window, name); 
	global_mon[i] = gtk_toggle_button_get_active(button);
//	printf ("global_mon_%i = %i\n", i, global_mon[i]);
	
    }
}


void
on_aux1_toggled                        (GtkToggleButton *togglebutton,
                                        gpointer         user_data)
{
    int i;
    GtkToggleButton *button;
    char name[256];
    
    for (i=0; i < N_PLUGINS; i++) {
	snprintf(name, 255, "aux1_%i", i+1);
	button = lookup_widget(main_window, name); 
	global_aux1[i] = gtk_toggle_button_get_active(button);
//	printf ("global_aux1_%i = %i\n", i, global_aux1[i]);
	
    }

}


void
on_aux2_toggled                        (GtkToggleButton *togglebutton,
                                        gpointer         user_data)
{
    int i;
    GtkToggleButton *button;
    char name[256];
    
    for (i=0; i < N_PLUGINS; i++) {
	snprintf(name, 255, "aux2_%i", i+1);
	button = lookup_widget(main_window, name); 
	global_aux2[i] = gtk_toggle_button_get_active(button);
//	printf ("global_aux2_%i = %i\n", i, global_aux2[i]);
	
    }

}

void
on_group_clicked                       (GtkToggleButton *togglebutton,
                                        gpointer         user_data)
{
    int g, i;
    GtkToggleButton *button;
    char name[256];
    
    for (g=0; g < 4; g++) {
	for (i=0; i < 6; i++) {
	    snprintf(name, 255, "g%i_%i",g+1, i+1);
	    button = lookup_widget(main_window, name); 
	    global_group[g][i] = gtk_toggle_button_get_active(button);
//	    printf ("global_group[%i][%i] = %i\n", g, i, global_group[g][i]);
	}
    }
}


gboolean
on_EQ_reset_button_release_event       (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data)
{

  return FALSE;
}




void
on_jack_ports_activate                 (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
    iomenu_pull_down_ports();
}



gboolean
on_label_button_press_event            (GtkWidget       *widget,
                                        int buttonPos,
                                        int buttonId)
{
//    printf ("Received click signal.\n");
    GdkColor audioOn, audioOff, white;
    GtkWidget *childWidget;
    char name[256];
    int i;

    

    audioOn.red = 41355;
    audioOn.green = 43000;
    audioOn.blue = 42355;
    
    audioOff.red = 65355;
    audioOff.green = 8000;
    audioOff.blue = 8355;
    
    white.red = 65355;
    white.green = 65355;
    white.blue = 65355;
    
    switch(buttonId)
    {
        case 1:
            childWidget = lookup_widget(main_window, "monitor_label");
        break;
        case 2:
            childWidget = lookup_widget(main_window, "master_label");
        break;
        case 3:
            childWidget = lookup_widget(main_window, "channel_1_label");
        break;
        case 4:
            childWidget = lookup_widget(main_window, "channel_2_label");
        break;
        case 5:
            childWidget = lookup_widget(main_window, "channel_3_label");
        break;
        case 6:
           childWidget = lookup_widget(main_window, "channel_4_label");
        break;
        case 7:
            childWidget = lookup_widget(main_window, "aux_1_label");
        break;
        case 8:
            childWidget = lookup_widget(main_window, "aux_2_label");
        break;


    
    }
    
 
    
   /* single click with the right mouse button? */
        switch(buttonPos)
        {
        
        case 3: // Right Click
         //   printf ("right click on label.\n");
         //   parentWidget = gtk_widget_get_ancestor (widget, GTK_TYPE_VBOX);
            if(widget->state == GTK_STATE_ACTIVE)
            {
                gtk_widget_set_state(widget,GTK_STATE_NORMAL);
                gtk_widget_set_state(childWidget,GTK_STATE_NORMAL);
                gtk_widget_modify_fg (childWidget,GTK_STATE_NORMAL,  &audioOn);
                
                // turn off mute
                global_bypass[buttonId-1] = 0; 
                
            } else {    
            //    gtk_widget_modify_bg (widget,GTK_STATE_ACTIVE,  &audioOff);
                gtk_widget_modify_fg (childWidget,GTK_STATE_ACTIVE,  &audioOff);
                gtk_widget_set_state(widget,GTK_STATE_ACTIVE);
                gtk_widget_set_state(childWidget,GTK_STATE_ACTIVE);
                
                // turn on mute
                global_bypass[buttonId-1] = 1; 
                

                
                
                
            }
            
        break;
        case 1: // Left Click
           // printf ("left click on label.\n");
        
            switch(buttonId)
            {

                case 3:
                case 4:
                case 5:
                case 6:
                    
                    messageBox = create_dialog1();
                    childWidget = lookup_widget(messageBox, "messageLabel");
                    snprintf(name, 255, "Reset EQ's\nfor Channel %i",buttonId-2);
                    gtk_label_set_text(childWidget, name);
                    gtk_widget_show(messageBox);
    
                    switch (gtk_dialog_run (GTK_DIALOG (messageBox)))
                    {
                    case GTK_RESPONSE_OK:
                 
                        for (i=1; i <= 3; i++) {
                            snprintf(name, 255, "djeq%i_%i",buttonId-2, i);
                            childWidget = lookup_widget(main_window, name); 
                            gtk_range_set_value(GTK_RANGE(childWidget), 0.0f);
                            
                        // printf ("childWidget = %s\n",name);
                        }
                
                    gtk_widget_destroy (messageBox);
                    break;
                    default:
                    gtk_widget_destroy (messageBox);
                    return FALSE;
                    }  
                  
                break;
            }    
        
        break;
        }
        

  return FALSE;
}


gboolean
on_mute_eventbox_expose                (GtkWidget       *widget,
                                        GdkEventExpose  *event,
                                        gpointer         user_data)
{

    int height = 0, width = 0;
    cairo_t *cr;
    cairo_pattern_t *pat;	

  height = widget->allocation.height - 2;
  width = widget->allocation.width - 2;
  
  GTK_WIDGET_SET_FLAGS(widget,GTK_APP_PAINTABLE);
  
//  printf("length = %i width = %i",length, width);

// left border
  cr = gdk_cairo_create (widget->window);	
  cairo_rectangle (cr, 0, 0, 1, height+2);
  cairo_set_source_rgba (cr, 0.6,0.6,0.6, 1.0); 
  cairo_fill (cr);
  cairo_destroy(cr);
  // top border
  cr = gdk_cairo_create (widget->window);	
  cairo_rectangle (cr, 0, 0, width, 1);
  cairo_set_source_rgba (cr, 0.6,0.6,0.6, 1.0); 
  cairo_fill (cr);
  cairo_destroy(cr);  
// right border  
  cr = gdk_cairo_create (widget->window);	
  cairo_rectangle (cr, width-1, 1, 1, height-1);
  cairo_set_source_rgba (cr, 0.5,0.5,0.5, 1.0); 
  cairo_fill (cr);
  cairo_destroy(cr);
// bottom border
  cr = gdk_cairo_create (widget->window);	
  cairo_rectangle (cr, 0, height-1, width, 1);
  cairo_set_source_rgba (cr, 0.5,0.5,0.5, 1.0); 
  cairo_fill (cr);
  cairo_destroy(cr);    
  
   // left hand glass bubble effect  
  cr = gdk_cairo_create (widget->window); 
  pat = cairo_pattern_create_linear (0, 0, 0, height-4);
  cairo_pattern_add_color_stop_rgba (pat, 0, 0, 0.2, 0.7, 0.2);
    cairo_pattern_add_color_stop_rgba (pat, 0.5, 1.0, 1.0, 1.0, 0.3);
    cairo_pattern_add_color_stop_rgba (pat, 1.0, 0, 0.2, 0.7, 0.2);
    cairo_rectangle (cr, 2, 2, width-4, height-4);
    cairo_set_source (cr, pat);
    cairo_fill (cr);
    cairo_pattern_destroy (pat);
    cairo_destroy(cr);


  return FALSE;
}



gboolean
on_monlabel_button_press_event         (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data)
{


    on_label_button_press_event (widget, event->button, 1);

  return FALSE;
}


gboolean
on_masterlabel_button_press_event      (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data)
{

    on_label_button_press_event (widget, event->button, 2);
  return FALSE;
}


gboolean
on_channel1label_button_press_event    (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data)
{

    on_label_button_press_event (widget, event->button, 3);
  return FALSE;
}


gboolean
on_channel2label_button_press_event    (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data)
{

   on_label_button_press_event (widget, event->button, 4);

  return FALSE;
}


gboolean
on_channel3label_button_press_event    (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data)
{

    on_label_button_press_event (widget, event->button, 5);
  return FALSE;
}


gboolean
on_channel4label_button_press_event    (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data)
{

    on_label_button_press_event (widget, event->button, 6);
  return FALSE;
}


gboolean
on_aux1label_button_press_event        (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data)
{

    on_label_button_press_event (widget, event->button, 7);
  return FALSE;
}


gboolean
on_aux2label_button_press_event        (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data)
{

    on_label_button_press_event (widget, event->button, 8);
  return FALSE;
}


