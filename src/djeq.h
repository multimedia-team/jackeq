#ifndef DJEQ_H
#define DJEQ_H

#include "ladspa.h"
#include "plugin.h"

#define DJ_EQ_LO                       0
#define DJ_EQ_MID                      1
#define DJ_EQ_HI                       2
#define DJ_EQ_LEFT_INPUT               3
#define DJ_EQ_RIGHT_INPUT              4
#define DJ_EQ_LEFT_OUTPUT              5
#define DJ_EQ_RIGHT_OUTPUT             6
#define DJ_EQ_LATENCY		       7

typedef struct {
	float lo;
	float mid;
	float hi;
	float latency;
	LADSPA_Handle handle;
} djeq_settings;

static inline void djeq_connect(plugin *p, djeq_settings *s)
{
	plugin_connect_port(p, s->handle, DJ_EQ_LO, &(s->lo));
	plugin_connect_port(p, s->handle, DJ_EQ_MID, &(s->mid));
	plugin_connect_port(p, s->handle, DJ_EQ_HI, &(s->hi));
	plugin_connect_port(p, s->handle, DJ_EQ_LATENCY, &(s->latency));


	/* Make sure that it is set to something */
	s->lo = 0.0f;
	s->mid = 0.0f;
	s->hi = 0.0f;
}

// djeq_settings djeq_get_settings(int i);

#endif
