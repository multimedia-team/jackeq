/*
 * Initial main.c file generated by Glade. Edit as required.
 * Glade will not overwrite this file.
 */

#ifdef HAVE_CONFIG_H
#  include "config.h"
#endif

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <gtk/gtk.h>
#include <limits.h>
#include <getopt.h>
#include <glib.h>
#include <glib/gstdio.h>

#include "aux-ui.h"
#include "djeq-ui.h"
#include "master-ui.h"
#include "monitor-ui.h"
#include "interface.h"
#include "io-menu.h"
#include "support.h"
#include "process.h"
#include "help.h"
#include "callbacks.h"

#define JACKHOME    ".jackeq"
#define JACKCONF    "jackeq.conf"
#define JACKRC      "jackrc"

GtkWidget *main_window, *widget;
gchar filepath[255];
GdkColor background;

gboolean update_meters(gpointer data);

static sigset_t signals;

static void help()
{
	printf(
		"\n"
		"Usage: jackeq [options]\n"
		"\n"
		"Available options:\n"
		"\n"
		"  -d,--daemon		Daemon Mode\n"
		"  -g,--gui		gtk2 GUI Mode\n"
		"\n"
		"  -h,--help		Print this help message\n"
		"\n");
}



int main(int argc, char *argv[])
{
    char rcfile[PATH_MAX], title[128];
    int fd;
    int run;
    int sig;
    sigset_t allsignals;
    	
    int opt;
    int option_index;
    const char *options = "dgh";

    struct option long_options[] = {
	{ "daemon", 0, 0, 'd' },
	{ "gui", 0, 0, 'g' },
	{ "help", 0, 0, 'h' },
	{ 0, 0, 0, 0 }
    };
    
	sigemptyset (&signals);
	sigaddset(&signals, SIGHUP);
	sigaddset(&signals, SIGINT);
	sigaddset(&signals, SIGQUIT);
	sigaddset(&signals, SIGPIPE);
	sigaddset(&signals, SIGTERM);
	sigfillset (&allsignals);
		      
#ifdef ENABLE_NLS
    bindtextdomain(GETTEXT_PACKAGE, PACKAGE_LOCALE_DIR);
    bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8");
    textdomain(GETTEXT_PACKAGE);
#endif

    printf(PACKAGE " " VERSION "\n");
    printf("(c) 2003 - 2009 P. Shirkey\n");
    printf("Featuring the DJEQ ladspa plugin by S. Harris\n");
    printf("With assistance from J. O'Quin on the awesome Jack i/o dropdown menu\n");
    printf("This is free software, and you are welcome to redistribute it\n" 
	   "under certain conditions; see the file COPYING for details.\n");

	while ((opt = getopt_long(argc, argv, options, long_options, &option_index)) != EOF) {
	    switch(opt) {
		case 'd':

		    printf("Running in Daemon Mode\n"); 
		    
		    /***********************************************
		     *  Move Below code into a seperate file for handling daemon controls/OSC 
		     ***********************************************/
		    
		    int g, i;
		    
		    for (g=0; g < 4; g++) {
			for (i=0; i < 6; i++) {		    
			    global_group[g][i] = i;
			}
		    }	
		    
		    /************************************************
		     *  Move Above code into a seperate file for handling daemon controls/OSC 
		     ************************************************/
		    
		    process_init();
		    run = TRUE;

		    while (run){
			
//		    printf("Daemon loop 1\n");
		    
		    
/*		    Use Sigwait because JACK does 
 *		    and it also catches ^C from keyboard 
 */

		    sigwait(&signals, &sig);
			
		    }   	
			
		    	   	
		      
		return 0;
				
		case 'h':
		
		  help();
		  return 0;

	    }
	}


    /* look for the rcfile, if its there parse it */

    snprintf(rcfile, PATH_MAX, "%s/%s/%s", getenv("HOME"), JACKHOME, JACKRC);
    if ((fd = open(rcfile, O_RDONLY)) >= 0) {	
	close(fd);
	printf("Using jackeqrc file: %s\n", rcfile);
	gtk_rc_parse(rcfile);
    }


    gtk_set_locale();
    gtk_init(&argc, &argv);
    add_pixmap_directory(PACKAGE_DATA_DIR "/jackeq");
    main_window = create_window1();
    
    snprintf(title, sizeof(title), PACKAGE " " VERSION);
    gtk_window_set_title ((GtkWindow *) main_window, title);

    gtk_widget_show(main_window);
    bind_mast();
    get_mast_out_meter_value();
    bind_mon();
    bind_djeq();
    bind_aux();
    bind_iomenu();

    g_snprintf(filepath, 255, "%s/%s/%s", getenv("HOME"), JACKHOME, JACKCONF);
    g_print(_("loading default config file %s\n"), filepath); 
    getSettings();
    
/* Set background color for interface */    
    background.red = 45055;
    background.green = 47000;
    background.blue = 50355;
    
    gtk_widget_modify_bg (main_window, GTK_STATE_NORMAL,  &background);
    
/* Set font color for eq labels - etched */        
    int i,ii;
    char name[10];
    
/*    background.red = 25055;
    background.green = 27000;
    background.blue = 30355;
*/    
    background.red = 63535;
    background.green = 63535;
    background.blue = 63535;
    
    for(i=1;i<=4;i++)
    {
        for(ii=1;ii<=3;ii++)
        {
            snprintf(name, 10, "djeq%i_l%i", i, ii);
            widget = lookup_widget(main_window, name); 
            gtk_widget_modify_fg (widget, GTK_STATE_NORMAL,  &background);
        }
    }
    
    
    /* start I/O processing, then run GTK main loop, until "quit" */

    process_init();
    g_timeout_add(100, update_meters, NULL);
    gtk_main();
    

    return 0;	
	
		
	
}

gboolean update_meters(gpointer data)
{
//    static unsigned int    count = 0;
    
    aux_in_meter_value(aux_peak);
    mast_out_meter_value(mast_peak);
    mon_out_meter_value(mon_peak);
    out_meter_value(out_peak);

    /*  Only update the status once a second.  */

 //   if (count % 10) status_update (main_window);
 //   count++;


    return TRUE;
}


void getSettings()
{
//  Settings *conf;
  GKeyFile *keyfile;
  GKeyFileFlags flags;
  GError *error = NULL;
//  gsize length;
  GtkWidget *slider;
  GtkWidget *button;
  char name[255],channelName[255], conffile[PATH_MAX], vname[5];
  int i,g;
  
//  printf("in getSettings\n"); 
  
  /* Create a new GKeyFile object and a bitwise list of flags. */
  keyfile = g_key_file_new ();
  flags = G_KEY_FILE_KEEP_COMMENTS | G_KEY_FILE_KEEP_TRANSLATIONS;
  
  /* Load the GKeyFile from keyfile.conf or return. */
  if (!g_key_file_load_from_file (keyfile, filepath, flags, &error))
  {
    return;
    g_error (error->message);
  }
  
  //gdouble c1 = g_key_file_get_double (keyfile, "group1", "c1", NULL);
  
//  printf("in getSettings c1 = %f\n", c1); 
  
  for(i=1;i<=4;i++)
  {
        snprintf(name, 255, "channel_gain_%i", i);	
        slider = lookup_widget(main_window, name); 
        snprintf(channelName, 255, "chan%i", i);
        gtk_range_set_value(GTK_RANGE(slider), g_key_file_get_double (keyfile, channelName, "vol", NULL));
        
        snprintf(name, 255, "djeq%i_1", i);
        slider = lookup_widget(main_window, name); 
        gtk_range_set_value(GTK_RANGE(slider), g_key_file_get_double (keyfile, channelName, "hi", NULL));
        
        snprintf(name, 255, "djeq%i_2", i);
        slider = lookup_widget(main_window, name); 
        gtk_range_set_value(GTK_RANGE(slider), g_key_file_get_double (keyfile, channelName, "mid", NULL));
        
        snprintf(name, 255, "djeq%i_3", i);
        slider = lookup_widget(main_window, name); 
        gtk_range_set_value(GTK_RANGE(slider), g_key_file_get_double (keyfile, channelName, "lo", NULL));
  }
  
  for(i=1;i<=2;i++)
  {  
        snprintf(name, 255, "aux_gain_%i", i);	
        slider = lookup_widget(main_window, name);
        snprintf(vname, 5, "vol%i", i);	 
        gtk_range_set_value(GTK_RANGE(slider), g_key_file_get_double (keyfile, "aux", vname, NULL));
        
        snprintf(name, 255, "fader_%i", i);	
        slider = lookup_widget(main_window, name); 
        snprintf(vname, 5, "bal%i", i);	
        gtk_range_set_value(GTK_RANGE(slider), g_key_file_get_double (keyfile, "faders", vname, NULL));
  }
  

        slider = lookup_widget(main_window, "monitor"); 
        gtk_range_set_value(GTK_RANGE(slider), g_key_file_get_double (keyfile, "outs", "monitor", NULL));
        	
        slider = lookup_widget(main_window, "master"); 
        gtk_range_set_value(GTK_RANGE(slider), g_key_file_get_double (keyfile, "outs", "master", NULL));
  
  
    for(i=1;i<=6;i++)
  {  
        snprintf(name, 255, "mon_button_%i", i);	
        button = lookup_widget(main_window, name); 
        snprintf(name, 255, "monButton_%i", i);	
        gtk_toggle_button_set_active(button, g_key_file_get_boolean (keyfile, "monButtons", name, NULL));
        
  }
  
    for(i=1;i<=4;i++)
  {  
        snprintf(name, 255, "aux1_%i", i);	
        button = lookup_widget(main_window, name); 
        snprintf(name, 255, "aux1_%i", i);	
        gtk_toggle_button_set_active(button, g_key_file_get_boolean (keyfile, "auxButtons", name, NULL));
        
        snprintf(name, 255, "aux2_%i", i);	
        button = lookup_widget(main_window, name); 
        snprintf(name, 255, "aux2_%i", i);	
        gtk_toggle_button_set_active(button, g_key_file_get_boolean (keyfile, "auxButtons", name, NULL));
  }
  
  
   for (g=1; g <= 4; g++) {

            snprintf(name, 255, "g%i", g);
            i = g_key_file_get_integer (keyfile, "groupButtons", name, NULL);
	    snprintf(name, 255, "g%i_%i",g, i);
	    button = lookup_widget(main_window, name);
	    gtk_toggle_button_set_active(button, TRUE);
	    global_group[g][i] = TRUE;

//	    printf ("global_group[%i][%i] = %i\n", g, i, global_group[g][i]);

    } 
    
    for(i=1;i<=NCHANNELS;i++){
    
                
        snprintf(name, 255, "mute_eventbox%i", i);
        button = lookup_widget(main_window, name);
        
        snprintf(name, 255, "m%i",i);
        
        if( g_key_file_get_integer (keyfile, "muteButtons", name, NULL) == 1)
        {
            on_label_button_press_event (button, 3, i);
        
        } else {
        // Cheap trick to set button state at startup
        // Just toggle mute twice to set button state
            on_label_button_press_event (button, 3, i);
            on_label_button_press_event (button, 3, i);
        }

        
    }
  
  /* Create a new Settings object. If you are using GTK+ 2.8 or below, you should
   * use g_new() or g_malloc() instead! */
  //conf = g_slice_new (Settings);
  
  /* Read in data from the key file from the group "username". */
//  conf->name = g_key_file_get_string             (keyfile, "group1", 
//                                                  "c1", NULL);
/*  conf->hello = g_key_file_get_locale_string     (keyfile, "username", 
                                                  "Hi", "es", NULL);
  conf->boolean = g_key_file_get_boolean_list    (keyfile, "username",  
                                                  "Bool", &length, NULL);
  conf->nums = g_key_file_get_integer_list       (keyfile, "username",  
                                                  "Nums", &length, NULL);
  conf->strings = g_key_file_get_string_list     (keyfile, "username",  
                                                  "Strings", &length, NULL);
  conf->meaning_of_life = g_key_file_get_integer (keyfile, "username",  
                                                  "Int", NULL);
  conf->doubles = g_key_file_get_double_list     (keyfile, "username",  
                                                  "Doubles", &length, NULL);
  
  */
 
    




}

void commitSettings()
{
//  Settings *conf;
  GKeyFile *keyfile;
  GKeyFileFlags flags;
  GError *error = NULL;
//  gsize length;
  GtkWidget *slider;
  GtkWidget *button;
  char name[256],channelName[255], vname[5];
  int i,g;
  
//  printf("in commitSettings\n"); 
  
  /* Create a new GKeyFile object and a bitwise list of flags. */
  keyfile = g_key_file_new ();
  flags = G_KEY_FILE_KEEP_COMMENTS | G_KEY_FILE_KEEP_TRANSLATIONS;
  
  //gdouble c1 = g_key_file_get_double (keyfile, "group1", "c1", NULL);
  
//  printf("in getSettings c1 = %f\n", c1); 
  
  for(i=1;i<=4;i++)
  {
        snprintf(name, 255, "channel_gain_%i", i);	
        slider = lookup_widget(main_window, name); 
        snprintf(channelName, 255, "chan%i", i);
        g_key_file_set_double (keyfile, channelName, "vol", gtk_range_get_value(GTK_RANGE(slider)));
        
  //      printf("in commitSettings c%i = %f\n",i, gtk_range_get_value(GTK_RANGE(slider))); 
        
        snprintf(name, 255, "djeq%i_1", i);
        slider = lookup_widget(main_window, name); 
        g_key_file_set_double (keyfile, channelName, "hi", gtk_range_get_value(GTK_RANGE(slider)));
        
        snprintf(name, 255, "djeq%i_2", i);
        slider = lookup_widget(main_window, name); 
        g_key_file_set_double (keyfile, channelName, "mid", gtk_range_get_value(GTK_RANGE(slider)));
        
        snprintf(name, 255, "djeq%i_3", i);
        slider = lookup_widget(main_window, name); 
        g_key_file_set_double (keyfile, channelName, "lo", gtk_range_get_value(GTK_RANGE(slider)));
  }
  
    for(i=1;i<=2;i++)
  {  
        snprintf(name, 255, "aux_gain_%i", i);	
        slider = lookup_widget(main_window, name); 
        snprintf(vname, 5, "vol%i", i);	
        g_key_file_set_double (keyfile, "aux", vname, gtk_range_get_value(GTK_RANGE(slider)));
        
        snprintf(name, 255, "fader_%i", i);	
        slider = lookup_widget(main_window, name); 
        snprintf(vname, 5, "bal%i", i);	
        g_key_file_set_double (keyfile, "faders", vname, gtk_range_get_value(GTK_RANGE(slider)));
  }
  

        slider = lookup_widget(main_window, "monitor"); 
        g_key_file_set_double (keyfile, "outs", "monitor", gtk_range_get_value(GTK_RANGE(slider)));
        	
        slider = lookup_widget(main_window, "master"); 
        g_key_file_set_double (keyfile, "outs", "master", gtk_range_get_value(GTK_RANGE(slider)));
  
  for(i=1;i<=6;i++)
  {  
        snprintf(name, 255, "mon_button_%i", i);	
        button = lookup_widget(main_window, name); 
        snprintf(name, 255, "monButton_%i", i);	
        g_key_file_set_boolean (keyfile, "monButtons", name, gtk_toggle_button_get_active(button));
        
  }
  
    for(i=1;i<=4;i++)
  {  
        snprintf(name, 255, "aux1_%i", i);	
        button = lookup_widget(main_window, name); 
        snprintf(name, 255, "aux1_%i", i);	
        g_key_file_set_boolean (keyfile, "auxButtons", name, gtk_toggle_button_get_active(button));
        
        snprintf(name, 255, "aux2_%i", i);	
        button = lookup_widget(main_window, name); 
        snprintf(name, 255, "aux2_%i", i);	
        g_key_file_set_boolean (keyfile, "auxButtons", name, gtk_toggle_button_get_active(button));
  }
  
 // printf("in commitSettings 1"); 
   for (g=1; g <= 4; g++) {
	for (i=1; i <= 6; i++) {
	    snprintf(name, 255, "g%i_%i",g, i);
	    button = lookup_widget(main_window, name);
	    if(gtk_toggle_button_get_active(button))
	 //   if(global_group[g][i])
	    {
	       snprintf(name, 255, "g%i",g);
	       g_key_file_set_integer (keyfile, "groupButtons", name, i);
	    } 
//	    printf ("global_group[%i][%i] = %i\n", g, i, global_group[g][i]);
	}
    } 
    
    for(i=0;i<NCHANNELS;i++){
    
        snprintf(name, 255, "m%i",i+1);
        g_key_file_set_integer (keyfile, "muteButtons", name,global_bypass[i]);
        
    }
    
    
 // printf("in commitSettings 2"); 
  storeGKeyFile(keyfile);
  g_key_file_free(keyfile);
  
  /* Create a new Settings object. If you are using GTK+ 2.8 or below, you should
   * use g_new() or g_malloc() instead! */
  //conf = g_slice_new (Settings);
  
  /* Read in data from the key file from the group "username". */
//  conf->name = g_key_file_get_string             (keyfile, "group1", 
//                                                  "c1", NULL);
/*  conf->hello = g_key_file_get_locale_string     (keyfile, "username", 
                                                  "Hi", "es", NULL);
  conf->boolean = g_key_file_get_boolean_list    (keyfile, "username",  
                                                  "Bool", &length, NULL);
  conf->nums = g_key_file_get_integer_list       (keyfile, "username",  
                                                  "Nums", &length, NULL);
  conf->strings = g_key_file_get_string_list     (keyfile, "username",  
                                                  "Strings", &length, NULL);
  conf->meaning_of_life = g_key_file_get_integer (keyfile, "username",  
                                                  "Int", NULL);
  conf->doubles = g_key_file_get_double_list     (keyfile, "username",  
                                                  "Doubles", &length, NULL);
  
  */
 
    




}

/*
 * Internal helper: saves the config.ini file to disk.
 * If configured to, user-prompt is created so that automatic saving of state is an option.
 * If storage fails, quietly informs user via the command line - not by pop-up, as this could
 * cause an annoyance to users.
 */
void storeGKeyFile(GKeyFile *keyString)
{
  gsize length;
  gchar *outText;
  GError *error = NULL;
  gchar *folderName;
//  GtkWidget *okCancelDialog;
//  char conffile[PATH_MAX];
/*  if (g_key_file_has_key(keyString, "configuration", "configPromptSave", NULL) &&
      (g_key_file_get_boolean(keyString, "configuration", "configPromptSave", NULL))) {
    okCancelDialog = gtk_message_dialog_new(GTK_WINDOW(mainWindow1), (GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT),
                                            GTK_MESSAGE_WARNING, GTK_BUTTONS_OK_CANCEL,
                                            _("About to overwrite save configuration. Do you want to proceed?"));
    if (gtk_dialog_run(GTK_DIALOG(okCancelDialog)) != GTK_RESPONSE_OK) {
      gtk_widget_destroy(okCancelDialog);
      return;
    }
    gtk_widget_destroy(okCancelDialog);
  }
*/    
  /* Write the configuration file to disk */

// printf("in storeGKeyFile"); 

  folderName = g_path_get_dirname(filepath);
  outText = g_key_file_to_data (keyString, &length, NULL);
  g_file_set_contents (filepath, outText, length, NULL);

 //   g_print(_("storeGKeyFile: writing filepath = %s\n"), filepath); 
    
  if (!g_file_set_contents(filepath, outText, length, NULL)) {

    /* Unable to immediately write to file, so attempt to recreate folders */
    mkFullDir(folderName, S_IRWXU);
    if (!g_file_set_contents (filepath, outText, length, &error)) { 
      g_print(_("Error saving %s: %s\n"), filepath, error->message);
      g_error_free(error);
      error = NULL;
    }
  }

  g_free(outText);
  g_free(folderName);
}


/*
 * Internal helper: equivalent to mkdir -p on linux.
 * Will recursively create missing folders to make the required foldername, and mode.
 * Always returns true!
 */
gboolean mkFullDir(gchar *folderName, gint mode)
{
  gchar *partialFolderName, *pPartialFolderName;
  gchar **folderParts;
  gint i = 0;
  
  /* Completely split folderName into parts */
  folderParts = g_strsplit_set(folderName, G_DIR_SEPARATOR_S, -1);
  partialFolderName = g_strdup(folderName);
  pPartialFolderName = partialFolderName;

  while (folderParts[i] != NULL) {
    pPartialFolderName = g_stpcpy(pPartialFolderName, folderParts[i]);
    pPartialFolderName = g_stpcpy(pPartialFolderName, G_DIR_SEPARATOR_S);
    
    if (!g_file_test (partialFolderName, G_FILE_TEST_IS_DIR)) {
      g_mkdir(partialFolderName, mode);
    }
    i++;
  }
  
  return TRUE;
}
