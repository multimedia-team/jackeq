/* this code is for the master gain and meters and the faders */

#include <stdio.h>
#include <gtk/gtk.h>
#include <math.h>

#include "process.h"
#include "support.h"
#include "main.h"
#include "gtkmeter.h"
#include "db.h"

#define M_GAIN  0


static GtkMeter *out_meter[2], *fader[2];
static GtkAdjustment  *out_meter_adj[2];


void bind_mast()
{
    int g, i;
    GtkWidget *slider;
    GtkWidget *button;
    char name[10];
    
    slider = lookup_widget(main_window, "master"); 
    gtk_range_set_value(GTK_RANGE(slider), -35.0);
	
    out_meter[0] = GTK_METER(lookup_widget(main_window, "mastmeter_l"));
    out_meter[1] = GTK_METER(lookup_widget(main_window, "mastmeter_r"));
    out_meter_adj[0] = gtk_meter_get_adjustment(out_meter[0]);
    out_meter_adj[1] = gtk_meter_get_adjustment(out_meter[1]);
    gtk_adjustment_set_value(out_meter_adj[0], -70.0);
    gtk_adjustment_set_value(out_meter_adj[1], -70.0);
	
    fader[0] = lookup_widget(main_window, "fader_1");
    fader[1] = lookup_widget(main_window, "fader_2");
    gtk_range_set_value(GTK_RANGE(fader[0]), 0.0);
    gtk_range_set_value(GTK_RANGE(fader[1]), 0.0);

    for (g=0; g < 4; g++) {
	for (i=0; i < 6; i++) {
	    snprintf(name, sizeof(name), "g%i_%i",g+1, i+1);
	    button = lookup_widget(main_window, name); 
	    global_group[g][i] = gtk_toggle_button_get_active(button);
//	    printf ("global_group[%i][%i] = %i\n", g, i, global_group[g][i]);
	}
    } 
}

void mast_out_meter_value(float amp[])
{
    gtk_adjustment_set_value(out_meter_adj[0], lin2db(amp[0]));
    gtk_adjustment_set_value(out_meter_adj[1], lin2db(amp[1]));
    amp[0] = 0.0f;
    amp[1] = 0.0f;
}


void mast_gain_changed(GtkAdjustment *adjustment, gpointer user_data)
{
    mast_gain_target = adjustment->value;
    
}


void get_mast_out_meter_value()
{
    printf ("%i, %i \n", out_meter_adj[0], out_meter_adj[1]);
    
}

/* vi:set ts=8 sts=4 sw=4: */
