#include <gtk/gtk.h>

void
on_quit_button_clicked                        (GtkButton       *button,
                                        gpointer         user_data);

gboolean
on_window1_delete_event                (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data);

void
on_window1_show                        (GtkWidget       *widget,
                                        gpointer         user_data);

void
on_bypass_button_toggled               (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

GtkWidget*
make_meter (gchar *widget_name, gchar *string1, gchar *string2,
                gint int1, gint int2);

GtkWidget*
make_mscale (gchar *widget_name, gchar *string1, gchar *string2,
		gint int1, gint int2);

void
on_autoutton1_toggled                  (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

void
on_autoutton2_toggled                  (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

void
on_autoutton3_toggled                  (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

void
on_button11_clicked                    (GtkButton       *button,
                                        gpointer         user_data);

void
on_button12_clicked                    (GtkButton       *button,
                                        gpointer         user_data);

void
on_output_activate                     (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_undo_button_clicked                    (GtkButton       *button,
                                        gpointer         user_data);

void
on_save_button_clicked                 (GtkButton       *button,
                                        gpointer         user_data);

void
on_load_button_clicked                 (GtkButton       *button,
                                        gpointer         user_data);

void
on_optionmenu1_realize                 (GtkWidget       *widget,
                                        gpointer         user_data);

void
on_help_button_clicked                 (GtkButton       *button,
                                        gpointer         user_data);

gboolean
on_show_help                           (GtkWidget       *widget,
                                        GtkWidgetHelpType  help_type,
                                        gpointer         user_data);

gboolean
on_input_eventbox_enter_notify_event   (GtkWidget       *widget,
                                        GdkEventCrossing *event,
                                        gpointer         user_data);

gboolean
on_help_button_enter_notify_event      (GtkWidget       *widget,
                                        GdkEventCrossing *event,
                                        gpointer         user_data);

gboolean
on_bypass_button_enter_notify_event    (GtkWidget       *widget,
                                        GdkEventCrossing *event,
                                        gpointer         user_data);

gboolean
on_load_button_enter_notify_event      (GtkWidget       *widget,
                                        GdkEventCrossing *event,
                                        gpointer         user_data);

gboolean
on_save_button_enter_notify_event      (GtkWidget       *widget,
                                        GdkEventCrossing *event,
                                        gpointer         user_data);

gboolean
on_undo_button_enter_notify_event      (GtkWidget       *widget,
                                        GdkEventCrossing *event,
                                        gpointer         user_data);

gboolean
on_quit_button_enter_notify_event      (GtkWidget       *widget,
                                        GdkEventCrossing *event,
                                        gpointer         user_data);

gboolean
on_window1_key_press_event             (GtkWidget       *widget,
                                        GdkEventKey     *event,
                                        gpointer         user_data);

void
on_new1_activate                       (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_open1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_save1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_save_as1_activate                   (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_quit1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_cut1_activate                       (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_copy1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_paste1_activate                     (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_delete1_activate                    (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_about1_activate                     (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_Hi_scale_value_changed              (GtkRange        *range,
                                        gpointer         user_data);

void
on_Mid_scale_value_changed             (GtkRange        *range,
                                        gpointer         user_data);

void
on_Lo_scale_value_changed              (GtkRange        *range,
                                        gpointer         user_data);
void
on_mon_scale_value_changed             (GtkRange        *range,
                                        gpointer         user_data);

void
on_mon_button_toggled                  (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

void
on_master_scale_value_changed          (GtkRange        *range,
                                        gpointer         user_data);

void
on_channel_gain_scale_value_changed    (GtkRange        *range,
                                        gpointer         user_data);

void
on_optionmenu6_clicked                 (GtkButton       *button,
                                        gpointer         user_data);

void
on_optionmenu6_realize                 (GtkWidget       *widget,
                                        gpointer         user_data);

void
on_optionmenu1_realize                 (GtkWidget       *widget,
                                        gpointer         user_data);

void
on_optionmenu1_clicked                 (GtkButton       *button,
                                        gpointer         user_data);

void
on_optionmenu5_realize                 (GtkWidget       *widget,
                                        gpointer         user_data);

void
on_optionmenu5_clicked                 (GtkButton       *button,
                                        gpointer         user_data);

void
on_optionmenu3_realize                 (GtkWidget       *widget,
                                        gpointer         user_data);

void
on_optionmenu3_clicked                 (GtkButton       *button,
                                        gpointer         user_data);

void
on_mon_menu_realize                    (GtkWidget       *widget,
                                        gpointer         user_data);

void
on_mon_menu_clicked                    (GtkButton       *button,
                                        gpointer         user_data);

void
on_mast_menu_realize                   (GtkWidget       *widget,
                                        gpointer         user_data);

void
on_mast_menu_clicked                   (GtkButton       *button,
                                        gpointer         user_data);

GtkWidget*
on_io_menu_create (gchar *widget_name, gchar *string1, gchar *string2,
                gint int1, gint int2);


void
on_channel_gain_value_changed          (GtkRange        *range,
                                        gpointer         user_data);

void
on_channel_gain_scale_value_changed    (GtkRange        *range,
                                        gpointer         user_data);


void
on_fader_scale_value_changed           (GtkRange        *range,
                                        gpointer         user_data);


void
on_group_clicked			(GtkToggleButton *togglebutton,
                                        gpointer         user_data);


gboolean
on_EQ_reset_button_release_event       (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data);

void
on_aux_gain_value_changed              (GtkRange        *range,
                                        gpointer         user_data);

void
on_aux_gain_scale_value_changed        (GtkRange        *range,
                                        gpointer         user_data);

void
on_aux1_toggled                        (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

void
on_aux2_toggled                        (GtkToggleButton *togglebutton,
                                        gpointer         user_data);
void
on_left_menubar_button_press_event     (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_menubar6_activate_current           (GtkMenuShell    *menushell,
                                        gboolean         force_hide,
                                        gpointer         user_data);

void
on_jack_ports_activate                 (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_open2_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_save2_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

gboolean
on_label_button_press_event            (GtkWidget       *widget,
                                        int buttonPos,
                                        int buttonId);

gboolean
on_monlabel_button_press_event         (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data);

gboolean
on_masterlabel_button_press_event      (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data);

gboolean
on_channel1label_button_press_event    (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data);

gboolean
on_channel2label_button_press_event    (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data);

gboolean
on_channel3label_button_press_event    (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data);

gboolean
on_channel4label_button_press_event    (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data);

gboolean
on_aux1label_button_press_event        (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data);

gboolean
on_aux2label_button_press_event        (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data);

gboolean
on_mute_eventbox_expose                (GtkWidget       *widget,
                                        GdkEventExpose  *event,
                                        gpointer         user_data);


