#ifndef DB_H
#define DB_H

#define db2lin(g) (powf(10.0f, (g) * 0.05f))

#define lin2db(v) (20.0f * log10f(v))

#endif
