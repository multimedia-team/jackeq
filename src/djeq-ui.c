/* The meters and sliders for each channel are defined here */


#include <stdio.h>
#include <gtk/gtk.h>
#include <math.h>

#include "process.h"
#include "support.h"
#include "main.h"
#include "gtkmeter.h"
#include "db.h"

#define B_LO  0
#define B_MID 1
#define B_HI  2

void chan_gain_changed(int i, float value);


GtkWidget *slider;
static GtkMeter *out_meter_l[N_PLUGINS], *out_meter_r[N_PLUGINS];
static GtkAdjustment  *out_meter_adj_l[N_PLUGINS], *out_meter_adj_r[N_PLUGINS];

int i;

void bind_djeq()
{
    GtkWidget *slider;
    char name[256], lname[256], rname[256];
    
    for (i=0; i<N_PLUGINS; i++) {

	snprintf(name, 255, "channel_gain_%i", i+1);
	slider = lookup_widget(main_window, name); 
	gtk_range_set_value(GTK_RANGE(slider), 0.0f);
	
	snprintf(lname, 255, "outmeter_l_%i", i+1);
	snprintf(rname, 255, "outmeter_r_%i", i+1);
	out_meter_l[i] = GTK_METER(lookup_widget(main_window, lname));
	out_meter_r[i] = GTK_METER(lookup_widget(main_window, rname));
	out_meter_adj_l[i] = gtk_meter_get_adjustment(out_meter_l[i]);
	out_meter_adj_r[i] = gtk_meter_get_adjustment(out_meter_r[i]);
	gtk_adjustment_set_value(out_meter_adj_l[i], -70.0);
	gtk_adjustment_set_value(out_meter_adj_r[i], -70.0);
    }
}

void out_meter_value(float amp[])
{
    for (i=0; i<NCHANNELS; i++) {
    /* Number of eq channels are always 1/2 NCHANNELS */
	int p = i / 2;
	if (i % 2 == 0){
	    gtk_adjustment_set_value(out_meter_adj_l[p], lin2db(amp[i]));
//	    printf ("set out_meter_l%i\n", i);
	}else{
	    gtk_adjustment_set_value(out_meter_adj_r[p], lin2db(amp[i]));
//	    printf ("set out_meter_r%i\n", i);
	}
        amp[i] = 0.0f;
    }
}

void gain_changed(GtkAdjustment *adjustment, gpointer user_data)
{
    int band = (int)user_data;
    int i;

    for (i=0; i < N_PLUGINS; i++){
        switch (band) {
        case B_LO:
	   djeq[i].lo = adjustment->value;
	   break;
        case B_MID:
	    djeq[i].mid = adjustment->value;
	    break;
        case B_HI:
	    djeq[i].hi = adjustment->value;
	    break;
        default:
	    fprintf(stderr, "Bad band number\n");
	    break;
        }	
    }
}


djeq_settings djeq_get_settings(int i)
{
    return (djeq[i]);
}

/* vi:set ts=8 sts=4 sw=4: */
