#include <math.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <jack/jack.h>
#include <assert.h>
#include <glib.h>

#include "config.h"
#include "process.h"
#include "djeq.h"

#define LERP(f,a,b) ((a) + (f) * ((b) - (a)))

djeq_settings djeq[N_PLUGINS];

float aux_peak[4];
float mast_peak[2];
float mon_peak[2];
float out_peak[NCHANNELS];
float aux_gain[2];
float aux_gain_target[2];
float chan_gain[N_PLUGINS];
float chan_gain_target[N_PLUGINS];
float fader_gain[N_PLUGINS];
float fader_gain_target[N_PLUGINS];
float mast_gain = 1.0f;
float mast_gain_target = 1.0f;
float mon_gain = 1.0f;
float mon_gain_old = 1.0f;
float mon_gain_target = 1.0f;

jack_port_t *input_ports[NCHANNELS],
                   *output_ports[NCHANNELS],
		   *aux_return_input_ports[4],
		   *aux_return_output_ports[4],
                   *aux_send_output_ports[4],
                   *mast_output_ports[2],
                   *mon_output_ports[2];
jack_client_t *client;
    
int global_bypass[NCHANNELS];
int global_mon[N_PLUGINS + 2];
int global_aux1[N_PLUGINS];
int global_aux2[N_PLUGINS];
int global_group[4][6];

/* Data for plugins */
plugin *djeq_plugins[N_PLUGINS];

int process(jack_nframes_t nframes, void *arg);
void crossfade(	jack_nframes_t nframes);
void cleanup();


void process_init()
{
    unsigned int chan, i;
    char *client_name;
    float fs;

    /* register as a JACK client */
    client_name = strdup(PACKAGE);
    printf("Registering as %s\n", client_name);
  //  client = jack_client_new(client_name);
    client = jack_client_open(client_name, JackNoStartServer, NULL);
    if (client == 0) {
	fprintf(stderr, "%s: Cannot contact JACK server, is it running?\n",
		PACKAGE);
        exit(2);
    }

    fs = jack_get_sample_rate(client);

    /* setup plugins */
    plugin_init();
    for (i=0; i<N_PLUGINS; i++) {
	djeq_plugins[i] = plugin_load("dj_eq_1901.so");

        if (djeq_plugins[i] == NULL )  {
           fprintf(stderr, "Required plugin 'dj_eq_1901.so' missing.\n");
           exit(1);
        }
	
	djeq[i].handle = plugin_instantiate(djeq_plugins[i], fs);
	djeq_connect(djeq_plugins[i], &djeq[i]);
//	printf ("create/connect djeq%i\n", i);
	
    }

    jack_set_process_callback(client, process, NULL);
    jack_on_shutdown(client, cleanup, NULL);
    
/* master & monitor outputs */
    
    for (chan = 0; chan < 2; chan++) {
	char *mast_out_name;
	char *mon_out_name;

        if (chan % 2 == 0){
	    mast_out_name = g_strdup_printf("a.master-L");
	    mon_out_name = g_strdup_printf("b.monitor-L");
	}else{    
	    mast_out_name = g_strdup_printf("a.master-R");
	    mon_out_name = g_strdup_printf("b.monitor-R");
	}
	
        mast_output_ports[chan] = jack_port_register(client,mast_out_name,
                               JACK_DEFAULT_AUDIO_TYPE,
                               JackPortIsOutput, 0);
	
        mon_output_ports[chan] = jack_port_register(client,mon_out_name,
                               JACK_DEFAULT_AUDIO_TYPE,
                               JackPortIsOutput, 0);

	free(mast_out_name);
	free(mon_out_name);

        if (mast_output_ports[chan] == NULL || mon_output_ports[chan] == NULL) {
            fprintf(stderr, "%s: Cannot register JACK ports.", PACKAGE);
            exit(2);
        }
    }	    
   
/*  channel i/os */


    for (chan = 0; chan < NCHANNELS; chan++) {
        int i = chan / 2;
	char *in_name;
	char *out_name;
	
        if (chan % 2 == 0){
	    in_name = g_strdup_printf("c.%d-in-L", i + 1);
	    out_name = g_strdup_printf("c.%d-out-L", i + 1);
	}else{
	    in_name = g_strdup_printf("c.%d-in-R", i + 1);
	    out_name = g_strdup_printf("c.%d-out-R", i + 1);
	}

        input_ports[chan] = jack_port_register(client, in_name,
                               JACK_DEFAULT_AUDIO_TYPE,
                               JackPortIsInput, 0);
        output_ports[chan] = jack_port_register(client, out_name,
                               JACK_DEFAULT_AUDIO_TYPE,
                               JackPortIsOutput, 0);

	free(in_name);
	free(out_name);

        if (input_ports[chan] == NULL || output_ports[chan] == NULL) {
            fprintf(stderr, "%s: Cannot register JACK ports.", PACKAGE);
            exit(2);
        }
	
   }
    
    
/*  Auxillary i/os */


    for (chan = 0; chan < 4; chan++) {
        int i = chan / 2;
	char *aux_send_out_name; 
	
       if (chan % 2 == 0){
	    aux_send_out_name = g_strdup_printf("d.fx%d_send-L", i + 1);
	}else{
	    aux_send_out_name = g_strdup_printf("d.fx%d_send-R", i + 1);
	}
	
        aux_send_output_ports[chan] = jack_port_register(client, aux_send_out_name,
                               JACK_DEFAULT_AUDIO_TYPE,
                               JackPortIsOutput, 0);

	free(aux_send_out_name);

        if (aux_send_output_ports[chan] == NULL) {
            fprintf(stderr, "%s: Cannot register JACK ports.", PACKAGE);
            exit(2);
        }
	
   }

    for (chan = 0; chan < 4; chan++) {
        int i = chan / 2;
	char *aux_return_in_name;
	char *aux_return_out_name;
	
       if (chan % 2 == 0){
	    aux_return_in_name = g_strdup_printf("e.fx%d_return-in-L", i + 1);
	    aux_return_out_name = g_strdup_printf("e.fx%d_return-out-L", i + 1);

	}else{
	    aux_return_in_name = g_strdup_printf("e.fx%d_return-in-R", i + 1);
	    aux_return_out_name = g_strdup_printf("e.fx%d_return-out-R", i + 1);
	}

        aux_return_input_ports[chan] = jack_port_register(client, aux_return_in_name,
                               JACK_DEFAULT_AUDIO_TYPE,
                               JackPortIsInput, 0);
        aux_return_output_ports[chan] = jack_port_register(client, aux_return_out_name,
                               JACK_DEFAULT_AUDIO_TYPE,
                               JackPortIsOutput, 0);

	free(aux_return_in_name);
	free(aux_return_out_name);

        if (aux_return_input_ports[chan] == NULL || aux_return_output_ports[chan] == NULL) {
            fprintf(stderr, "%s: Cannot register JACK ports.", PACKAGE);
            exit(2);
        }
	
   }
    
    jack_activate(client);
}

int process(jack_nframes_t nframes, void *arg)
{
    jack_default_audio_sample_t *in[NCHANNELS],
    				*out[NCHANNELS], 
    				*aux_return_in[4], 
    				*aux_return_out[4],
				*aux_send_out[4], 
				*mon_out[2];
    float mon_out_temp[NCHANNELS][nframes];				
    float aux_out_temp[4][nframes];
    int a, i;
    unsigned int port, pos;

    /* get monitor output buffer addresses from JACK */
    
    for (port = 0; port < 2; port++) {
        mon_out[port] = jack_port_get_buffer(mon_output_ports[port], nframes);
//	printf ("found buffer address for mon%i\n", port);
	if (mon_out[port] == NULL) {
	    return 0;

	}
    }
    
    /* get aux input & output buffer addresses from JACK */
    
    for (port = 0; port < 4; port++) {
        aux_send_out[port] = jack_port_get_buffer(aux_send_output_ports[port], nframes);
//	printf ("found buffer address for out%i\n", port+1);
	
  	if (aux_send_out[port] == NULL) {
	    return 0;
	}
    }
    
    for (port = 0; port < 4; port++) {
        aux_return_in[port] = jack_port_get_buffer(aux_return_input_ports[port], nframes);
        aux_return_out[port] = jack_port_get_buffer(aux_return_output_ports[port], nframes);
	//	printf ("found buffer address for in%i\n", port+1);
	
  	if (aux_return_in[port] == NULL || aux_return_out[port] == NULL) {
	    return 0;
	}
    }
	    
    /* get channel input & output buffer addresses from JACK */
    
    for (port = 0; port < NCHANNELS; port++) {
    

        in[port] = jack_port_get_buffer(input_ports[port], nframes);
    //	printf ("found buffer address for in%i\n", port+1);
        out[port] = jack_port_get_buffer(output_ports[port], nframes);
    //	printf ("found buffer address for out%i\n", port+1);

  	if (in[port] == NULL || out[port] == NULL) {
	    return 0;
	}
    }
		
    /* connect each channel to a seperate DJ EQ */
 
    for (port = 0; port < NCHANNELS; port++) {
       i = port / 2;
	    
       if (port % 2 == 0){
            plugin_connect_port(djeq_plugins[i], djeq[i].handle, DJ_EQ_LEFT_INPUT, in[port]);
            plugin_connect_port(djeq_plugins[i], djeq[i].handle, DJ_EQ_LEFT_OUTPUT, out[port]);
//            printf ("connected left i/o djeq%i to c.%i-io%d\n", i, i+1, port+1);
	} else {
            plugin_connect_port(djeq_plugins[i], djeq[i].handle, DJ_EQ_RIGHT_INPUT, in[port]);
            plugin_connect_port(djeq_plugins[i], djeq[i].handle, DJ_EQ_RIGHT_OUTPUT, out[port]); 
//	    printf ("connected right i/o djeq%i to c.%i-io%d\n", i, i+1, port+1);
 	}
    }

/*  start the plugins processing audio */
    for (i=0; i< N_PLUGINS;i++){
	plugin_run(djeq_plugins[i], djeq[i].handle, nframes);
    }

/* This code sorts the channel i/o ports, sets the channel output meters to the correct gain level and routes all to the master & monitor gain controls and faders.
   - route presend to monitor so channel gain faders don't affect monitor signal level 
   - set the channel output volume 
*/

/*  zero the buffers first so we don't get gain feedback */

    for (pos = 0; pos < nframes; pos++) {
	mon_out[0][pos] = 0.0f;
	mon_out[1][pos] = 0.0f;
    }
//    printf ("zeroed mon_out buffers");

    for (port = 0; port < 4; port++) {
	for (pos = 0; pos < nframes; pos++) {
	    aux_send_out[port][pos] = 0.0f;
	}
    }	
//    printf ("zeroed aux_send_out buffers");
    
    for (port = 0; port < 4; port++) {
	for (pos = 0; pos < nframes; pos++) {
	    aux_return_out[port][pos] = 0.0f;
	}
    }	
//    printf ("zeroed aux_return_out buffers");

    for (port = 0; port < NCHANNELS; port++) {
	for (pos = 0; pos < nframes; pos++) {
	    mon_out_temp[port][pos] = 0.0f;
	}
    }	
//    printf ("zeroed mon_out_temp buffers");
    
    for (port = 0; port < NCHANNELS; port++) {
        i = port / 2;
        for (pos = 0; pos < nframes; pos++) {
	
/* Pre Gain 
   - All channels route to monitor but signal is muted by default.
   - unmutes port signal when monitor button is toggled 
   - mutes output signal when mute (global_bypass[0]) is enabled.
*/	    
	if (global_mon[i] == 1 && global_bypass[0] == 0) {
	    mon_out[port % 2][pos] += out[port][pos];
	}else{
	    mon_out[port % 2][pos] += 0.0f;
	}
		
	    
/* Set i/o channels gain */
	    chan_gain[i] = chan_gain[i] * 0.9f + chan_gain_target[i] * 0.1f;
	    
	if(global_bypass[i+2] == 1){ // Mute channels 1 - 4
            in[port][pos] = 0.0f;
            out[port][pos] = 0.0f;
        
        } else {
	    out[port][pos] = out[port][pos] * chan_gain[i];
//	    printf ("2. port%i oa = %f\n", port, oa);
        }
/* Route to aux channels 
    - Post channel fader gain.
    - Need to seperate routed i/os for each aux send/return
*/

	    if (global_aux1[i] == 1 && global_aux2[i] != 1) {
		if (port % 2 == 0){
		    aux_send_out[0][pos] += out[port][pos];
		}else{
		    aux_send_out[1][pos] += out[port][pos];
		}    
//	    printf ("1:routed c.%i-in%i to aux1\n", i+1, port);
		
	    } else if (global_aux2[i] == 1 && global_aux1[i] != 1) {
		if (port % 2 == 0){
		    aux_send_out[2][pos] += out[port][pos];
		}else{
		    aux_send_out[3][pos] += out[port][pos];
		} 	    
//	    printf ("2:routed c.%i-in%i to aux2\n", i+1, port);
		
	    } else if (global_aux2[i] == 1 && global_aux1[i] == 1) {
		if (port % 2 == 0){
		    aux_send_out[0][pos] += out[port][pos];
		    aux_send_out[2][pos] += out[port][pos];
		}else{
		    aux_send_out[1][pos] += out[port][pos];
		    aux_send_out[3][pos] += out[port][pos];
		} 	    
//	    printf ("2:routed c.%i-in%i to aux1 & aux2\n", i+1, port);

	    }else if (global_aux2[i] != 1 && global_aux1[i] != 1){
		for (a=0; a<4;a++){
		    aux_send_out[a][pos] += 0.0f;
		}
	    }
        }
//	    printf ("routed c.%i-in%i to monitor\n", i+1, port);
//	    printf ("set c.%i-in%i output volume\n", i+1, port);
//	    printf ("routed c.%i-in%i to aux\n", i+1, port);
	
    }
	
/*  aux meters 
   - sets the aux_return_output & meters to the correct level 
*/
    for (i = 0; i < 2; i++) {
	aux_gain[i] = aux_gain[i] * 0.85f + aux_gain_target[i] * 0.15f;
    }
    
    for (port = 0; port < 4; port++) {
	i = port / 2;

	for (pos = 0; pos < nframes; pos++) {
	    float oa; 
	    
	    if(global_bypass[i+6]==1){ // mute aux return - pre fader gain
	       aux_return_out[port][pos] = 0.0f;
	    } else {
	       aux_return_out[port][pos] =  aux_return_in[port][pos] * aux_gain[i];
	    }   
	    oa = fabs(aux_return_out[port][pos]);
	    
	    if (oa > aux_peak[port]) {
		aux_peak[port] = oa;
	    }

/* monitor buttons work on incoming signal pre gain */

	    if (global_mon[4] == 1 || global_mon[5] == 1) {
		mon_out[port % 2][pos] += aux_return_in[port][pos];
	    }else{
		mon_out[port % 2][pos] += 0.0f;
	    }
	    
//	    aux_send_out[port][pos] =  aux_out_temp[port][pos];
	    
	}
    }
	
//	    printf ("set aux output volume\n");

	
/*  monitor meters 
   - sets the monitor output & meters to the correct level 
*/


    mon_gain = mon_gain * 0.85f + mon_gain_target * 0.15f;

    for (port = 0; port < 2; port++) {
	for (pos = 0; pos < nframes; pos++) {
	    float oa; 

	    mon_out[port][pos] =  mon_out[port][pos] * mon_gain;

	    oa = fabs(mon_out[port][pos]);
	    
	    if (oa > mon_peak[port]) {
		mon_peak[port] = oa;
	    }	    
	}
    }
	
//	    printf ("set monitor output volume\n");

       
/*   
    - set each channels meter levels
    - route to xfader, channel sliders affect signal level 
*/

    for (port = 0; port < NCHANNELS; port++) {
        i = port / 2;
        for (pos = 0; pos < nframes; pos++) {
	    const float oa = fabs(out[port][pos]);
	    
	    if (oa > out_peak[port]) {
		out_peak[port] = oa;
	    } 
//	    printf ("oa = %f\n", oa);

	}   	
    }
	
/* Call crossfade function
    - route channel outputs to faders then master output 
*/ 

    crossfade(nframes);
	
    return 0;
}


void crossfade(jack_nframes_t nframes)
{

int f, g, i;
int group[4][2];
unsigned int pos, port;
float oa;
float a[2], xf[2];
float xfa[2], xfb[2];
float	fader_outA[1][nframes]; 
float	fader_outAA[1][nframes]; 
float	fader_outB[1][nframes];
float	fader_outBB[1][nframes];
float	all_out[2][nframes];

jack_default_audio_sample_t	*out[NCHANNELS], 
				*aux_return_out[4], 
				*mast_out[2]; 

mast_gain = mast_gain * 0.9f + mast_gain_target * 0.1f;

/* get output buffer addresses from JACK */

    for (port = 0; port < NCHANNELS; port++) {
	for (pos = 0; pos < nframes; pos++) {
	    out[port] = jack_port_get_buffer(output_ports[port], pos);
//	printf ("found buffer address for out%i\n", port);
	    if (out[port] == NULL) {
		return;
	    }
	}
    }
    
 /* get aux_return_output buffer addresses from JACK */

    for (port = 0; port < 4; port++) {
	for (pos = 0; pos < nframes; pos++) {
	    aux_return_out[port] = jack_port_get_buffer(aux_return_output_ports[port], pos);
//	printf ("found buffer address for out%i\n", port);
	    if (aux_return_out[port] == NULL) {
		return;
	    }
	}
    }
    
   
/* get master output buffer addresses from JACK */
    
    for (port = 0; port < 2; port++) {
//	for (pos = 0; pos < nframes; pos++) {
	    mast_out[port] = jack_port_get_buffer(mast_output_ports[port], nframes);
//	printf ("found buffer address for mast%i\n", port);
	    if (mast_out[port] == NULL) {
		return;
	    }
//	}
    }
/*  zero the buffers first so we don't get gain feedback */

    for (pos = 0; pos < nframes; pos++) {
	mast_out[0][pos] = 0.0f;
	mast_out[1][pos] = 0.0f;
	all_out[0][pos] = 0.0f;
	all_out[1][pos] = 0.0f;
    }
//    printf ("zeroed mon buffer");
    
/* 
    - find the channels that are being mixed for each side by scanning the groups for the active radio buttons then use that int to set input channels on each side.
    - 0 - 3 are channels 1 - 4.
    - 5(all) and 6(thru) are special cases.
    - g is group
    - i is button
*/
   
	for (g=0;g<4;g++){
	    for (i=0;i<6;i++){

		if (global_group[g][i] == 1){
/* buttons 1 - 4 */	
		    if (i < 4){
			group[g][0] = i * 2;
			group[g][1] = i * 2 + 1;
//    printf ("group[%i][0] = %i \n", g, group[g][0]);
//    printf ("group[%i][1] = %i \n", g, group[g][1]);
/* special case for button 5 (all) */	
		    }else if (i == 4){
			group[g][0] = NCHANNELS+1;
			group[g][1] = NCHANNELS+1;
/* special case for button 6 (mute) */	
		    }else if (i == 5){
			group[g][0] = NCHANNELS+2;
			group[g][1] = NCHANNELS+2;
		    }	
		}
//    printf ("i = %i\n", i);
	    }
//    printf ("group[%i][0] = %i \n", g, group[g][0]);
//    printf ("group[%i][1] = %i \n", g, group[g][1]);
	}
//    printf ("set fader_out ports for mixing\n");
	

/*  
    - f is the number of the fader. 
    - xf is the xfade between 0 and 1. 
    - The xfaders are -100 to 100 so we have to adjust fader_gain_target to get the correct value which we pass to xf[]
*/

    for (f=0; f<2;f++){
    
    a[f] = fader_gain_target[f];
	if(a[f]<0){
	    a[f] = a[f]/-200;
	    xf[f] = 0.5 - a[f];
	} else if(a[f]>0) {    
	    a[f] = a[f]/200;
	    xf[f] = 0.5 + a[f];
	} else {
	    xf[f] = 0.5;
	}
// printf ("adjust fader_gain_target xf%i=%f\n", f, xf[f]);

    xfa[f] = sqrtf(1.0f - xf[f]);
    xfb[f] = sqrtf(xf[f]);
    
//    printf ("xfa[%i] = %f\n", f, xfa[f]);
//    printf ("xfb[%i] = %f\n", f, xfb[f]);
}    

/* Special case for all button */

    for(port=0;port<NCHANNELS;port++){
	for(pos=0;pos<nframes;pos++){
	    all_out[port % 2][pos] += out[port][pos] * mast_gain;
	}	
    }
// printf ("Set fade all toggle");
 
/* do the mix for each i/o port on each fader */

/* set fader outputs */
    for (f=0; f<2;f++){
	for(pos=0;pos<nframes;pos++){
	    if(f % 2 == 0){
	    
/* Alot of this could probably be cut down but it does the job for now */
		
// printf ("Attempting to setup xf 1");
	    
/* fader1 */
		if(group[0][0] == NCHANNELS+2 && group[1][0] != NCHANNELS+2){
		    if(group[1][0] == NCHANNELS+1){
			fader_outA[0][pos] = xfa[f] * 0.0f + xfb[f] * all_out[0][pos];
			fader_outAA[0][pos] = xfa[f] * 0.0f + xfb[f] * all_out[1][pos];
		    }else{
			fader_outA[0][pos] = xfa[f] * 0.0f + xfb[f] * out[group[1][0]][pos];
			fader_outAA[0][pos] = xfa[f] * 0.0f + xfb[f] * out[group[1][1]][pos];
		    }	
		}else if(group[0][0] != NCHANNELS+2 && group[1][0] == NCHANNELS+2){    
		    if(group[0][0] == NCHANNELS+1){
			fader_outA[0][pos] = xfa[f] * all_out[0][pos] + xfb[f] * 0.0f;
			fader_outAA[0][pos] = xfa[f] * all_out[1][pos] + xfb[f] * 0.0f;
		    }else{
			fader_outA[0][pos] = xfa[f] * out[group[0][0]][pos] + xfb[f] * 0.0f;
			fader_outAA[0][pos] = xfa[f] * out[group[0][1]][pos] + xfb[f] * 0.0f;	
		    }	
		}else if(group[0][0] == NCHANNELS+2 && group[1][0] == NCHANNELS+2){    
		    fader_outA[0][pos] = xfa[f] * 0.0f + xfb[f] * 0.0f;
		    fader_outAA[0][pos] = xfa[f] * 0.0f + xfb[f] * 0.0f;	
		}else if(group[0][0] == NCHANNELS+1 && group[1][0] == NCHANNELS+1){    
		    fader_outA[0][pos] = xfa[f] * all_out[0][pos] + xfb[f] * all_out[0][pos];
		    fader_outAA[0][pos] = xfa[f] * all_out[1][pos] + xfb[f] * all_out[1][pos];	
		}else if(group[0][0] != NCHANNELS+2 && group[1][0] != NCHANNELS+2){    
		    if(group[0][0] == NCHANNELS+1){
			fader_outA[0][pos] = xfa[f] * all_out[0][pos] + xfb[f] * out[group[1][0]][pos];
			fader_outAA[0][pos] = xfa[f] * all_out[1][pos] + xfb[f] * out[group[1][1]][pos];
		    }else if(group[1][0] == NCHANNELS+1){
			fader_outA[0][pos] = xfa[f] * out[group[0][0]][pos] + xfb[f] * all_out[0][pos];
			fader_outAA[0][pos] = xfa[f] * out[group[0][1]][pos] + xfb[f] * all_out[1][pos];
		    }else if(group[0][0] != NCHANNELS+1 && group[1][0] != NCHANNELS+1){
			
/* Default procedure for 2 channels */
//    printf ("Attempting to set default fader_outA ports\n");
//    printf ("outgroup00 = %f\n",  out[0][pos] );
//    printf ("outgroup10 = %f\n",  out[2][pos] );

//    printf ("Set default fader_outAA ports\n");

			fader_outA[0][pos] = xfa[f] * out[group[0][0]][pos] + xfb[f] * out[group[1][0]][pos];
			fader_outAA[0][pos] = xfa[f] * out[group[0][1]][pos] + xfb[f] * out[group[1][1]][pos];

		    }
		}
//    printf ("set fader_outA ports\n");

	    } else{
	
/* fader2 */
		if(group[2][0] == NCHANNELS+2 && group[3][0] != NCHANNELS+2){
		    if(group[3][0] == NCHANNELS+1){
			fader_outB[0][pos] = xfa[f] * 0.0f + xfb[f] * all_out[0][pos];
			fader_outBB[0][pos] = xfa[f] * 0.0f + xfb[f] * all_out[1][pos];
		    }else{
			fader_outB[0][pos] = xfa[f] * 0.0f + xfb[f] * out[group[3][0]][pos];
			fader_outBB[0][pos] = xfa[f] * 0.0f + xfb[f] * out[group[3][1]][pos];
		    }	
		}else if(group[2][0] != NCHANNELS+2 && group[3][0] == NCHANNELS+2){ 
		    if(group[2][0] == NCHANNELS+1){
			fader_outB[0][pos] = xfa[f] * all_out[0][pos] + xfb[f] * 0.0f;
			fader_outBB[0][pos] = xfa[f] * all_out[1][pos] + xfb[f] * 0.0f;
		    }else{
			fader_outB[0][pos] = xfa[f] * out[group[2][0]][pos] + xfb[f] * 0.0f;
			fader_outBB[0][pos] = xfa[f] * out[group[2][1]][pos] + xfb[f] * 0.0f;
		    }	
		}else if(group[2][0] == NCHANNELS+2 && group[3][0] == NCHANNELS+2){ 
		    fader_outB[0][pos] = xfa[f] * 0.0f + xfb[f] * 0.0f;
		    fader_outBB[0][pos] = xfa[f] * 0.0f + xfb[f] * 0.0f;
		}else if(group[2][0] == NCHANNELS+1 && group[3][0] == NCHANNELS+1){    
		    fader_outB[0][pos] = xfa[f] * all_out[0][pos] + xfb[f] * all_out[0][pos];
		    fader_outBB[0][pos] = xfa[f] * all_out[1][pos] + xfb[f] * all_out[1][pos];	
		}else{    
		    if(group[0][0] == NCHANNELS+1){
			fader_outB[0][pos] = xfa[f] * all_out[0][pos] + xfb[f] * out[group[3][0]][pos];
			fader_outBB[0][pos] = xfa[f] * all_out[1][pos] + xfb[f] * out[group[3][1]][pos];
		    }else if(group[1][0] == NCHANNELS+1){
			fader_outB[0][pos] = xfa[f] * out[group[2][0]][pos] + xfb[f] * all_out[0][pos];
			fader_outBB[0][pos] = xfa[f] * out[group[2][1]][pos] + xfb[f] * all_out[1][pos];
		    }else {
/* Default procedure for 2 channels */
			
			fader_outB[0][pos] = xfa[f] * out[group[2][0]][pos] + xfb[f] * out[group[3][0]][pos];
			fader_outBB[0][pos] = xfa[f] * out[group[2][1]][pos] + xfb[f] * out[group[3][1]][pos];
		    }
		}
		    
//   printf ("set fader_outB ports\n");
	    }
	}
/* end xfade for{} loop */	
    }


/* Combine fader outputs and pass to master output channels */

//    mast_gain = mast_gain * 0.9f + mast_gain_target * 0.1f;
	
        for (pos = 0; pos < nframes; pos++) {
        
            if(global_bypass[1] == 1){ // mute master outputs
                mast_out[0][pos] = 0.0f;
                mast_out[1][pos] = 0.0f;
            } else {
                mast_out[0][pos] = (fader_outA[0][pos] + fader_outB[0][pos] + aux_return_out[0][pos] + aux_return_out[2][pos]) * mast_gain;
                mast_out[1][pos] = (fader_outAA[0][pos] + fader_outBB[0][pos] + aux_return_out[1][pos] + aux_return_out[3][pos]) * mast_gain;
    //    printf ("set mast_out ports\n");
}
	}

/*   - set the master meter levels */
     
    for(i=0;i<2;i++){
        for (pos = 0; pos < nframes; pos++) {
	    oa = fabs(mast_out[i][pos]);

	    if (oa > mast_peak[i]) {
		mast_peak[i] = oa;
	    } 
	}
    }	
//	    printf ("set master output meters\n");
	
    
}

void cleanup()
{
    jack_client_close(client);
}

/* vi:set ts=8 sts=4 sw=4: */



