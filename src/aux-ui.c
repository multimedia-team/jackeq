/* The meters and sliders for each aux channel are defined here */


#include <stdio.h>
#include <gtk/gtk.h>
#include <math.h>

#include "process.h"
#include "support.h"
#include "main.h"
#include "gtkmeter.h"
#include "db.h"

// void aux_gain_changed(int i, float value);


GtkWidget *slider;
static GtkMeter *aux_meter_l[2], *aux_meter_r[2];
static GtkAdjustment  *aux_meter_adj_l[2], *aux_meter_adj_r[2];

int i;

void bind_aux()
{
    GtkWidget *slider;
    char name[256], lname[256], rname[256];
    
    for (i=0; i<2; i++) {

	snprintf(name, 255, "aux_gain_%i", i+1);
	slider = lookup_widget(main_window, name); 
	gtk_range_set_value(GTK_RANGE(slider), 0.0f);
//	printf ("set aux_gain%i\n", i);
	
	snprintf(lname, 255, "auxmeter_l_%i", i+1);
	snprintf(rname, 255, "auxmeter_r_%i", i+1);
	aux_meter_l[i] = GTK_METER(lookup_widget(main_window, lname));
	aux_meter_r[i] = GTK_METER(lookup_widget(main_window, rname));
	aux_meter_adj_l[i] = gtk_meter_get_adjustment(aux_meter_l[i]);
	aux_meter_adj_r[i] = gtk_meter_get_adjustment(aux_meter_r[i]);
	gtk_adjustment_set_value(aux_meter_adj_l[i], -70.0);
	gtk_adjustment_set_value(aux_meter_adj_r[i], -70.0);
    }
}

void aux_in_meter_value(float amp[])
{
    for (i=0; i<4; i++) {
    /* Number of aux meters(m) are always 1/2 aux ports(i) */
	int m = i / 2;
	
	if (i % 2 == 0){
	    gtk_adjustment_set_value(aux_meter_adj_l[m], lin2db(amp[i]));
//	    printf ("set aux_meter_l%i\n", i);
	}else{
	    gtk_adjustment_set_value(aux_meter_adj_r[m], lin2db(amp[i]));
//	    printf ("set aux_meter_r%i\n", i);
	}
        amp[i] = 0.0f;
    }
}

/* vi:set ts=8 sts=4 sw=4: */
